# -*-coding:utf-8 -*
#!/usr/bin/python
#
#_________________________________________________________________________
# Dzevel ROGOVIC
#
# WEB  SERVICE  -  API Invoice example
# WEB  SERVICE  -  API Facture exemple
# version v1.0.1
#
#_________________________________________________________________________
#
# Python 2.7
# Flask request object + flask_restful
# actions : +, -, *, /
# ---------------------------------------------------
# ATTENTION !!! WINDOWS
# simple quote --> double quote     et aussi     " --> \"
#
# curl -X POST -H "Content-Type: application/json" -d"{\"invoice\":{\"client\":\"client1\",
# \"node\":\"node1\",\"var1\":\"2\",\"var2\":\"5\",\"action\":\"+\"}}" http://127.0.0.1:5000/
# ---------------------------------------------------
## curl -X POST -H "Content-Type: application/json" -d'{"invoice":{"client":"client1",
# "node":"node1","var1":"2","var2":"5","action":"+"}}' http://127.0.0.1:5000/
#__________________________________________________________________________
# IMPORT LIBS
#__________________________________________________________________________

import os, sys, os.path, glob
import time
import random, string
import flask
import flask_restful
import logging, tempfile
import re

from flask import Flask, render_template, request, g, session, url_for, abort
from flask import json, jsonify, Response, make_response
from functools import wraps
from flask.ext import restful
from flask.ext.restful import abort
from flask_restful import fields, marshal, OrderedDict

from datetime import datetime

#______________________________________________________________________________
# CONSTANTES
#______________________________________________________________________________

VERSION = "0.0.1"

ZKEY_SIZE = 32

ZUSERPW_DADMIN = "admin:admin"

#------------------------------------
# STRUCTURE JSON INPUT MODELE / request client -> server
#------------------------------------

JSON_IN = {"invoice": {"client": "",
                       "node": "",
                       "var1": "",
                       "var2": "",
                       "action": ""
                       }
           }
#------------------------------------
# STRUCTURE JSON OUTPUT MODELE / response server -> client
#------------------------------------
#
#JSON_OUT_MODEL =
# -- CREATE --
#{
#  "invoice": {
#    "node": [
#      {
#        "client": "",
#		 "node": "",
#        "var1": "",
#        "var2": "",
#        "resultat": "",
#        "status": 200,
#        "message":"OK",
#        "action": ""
#      }
#    ]
#  }
#}
# -- DIVERS --
#{
#    "message": "Bad Request",
#    "status": 400
#}
#{
#    "message": "Unauthorized",
#    "status": 401
#}
#______________________________________________________________________________
# INIT flask-restful
#______________________________________________________________________________

APP = Flask(__name__)
API = flask_restful.Api(APP)

APP.debug = True

APP.secret_key = "apkfuè'(àfnjkie25678h25de"

FILE_HANDLER = logging.FileHandler('invoice.log')
APP.logger.addHandler(FILE_HANDLER)
APP.logger.setLevel(logging.INFO)

#______________________________________________________________________________
# JSON VERIFICATION
#______________________________________________________________________________
def client_jsonrequest_verif(myjsonin):
    """ def client_jsonrequest_verif
        JSON VERIFICATION

        JSON_IN -->> Python dict
        mydata = creation objet python a partir de json_in
        mydata2 = creation string python objet pour lire python instructions
    """
    try:
        APP.logger.info('def client_jsonrequest_verif ')
        mydatain = json.dumps(myjsonin)
        mydata2in = json.loads(mydatain)

        # --verification si json_in  ok compare avec JSON_IN param dans fichier

        for cle, valeur in JSON_IN.items():
            print cle, valeur
            if cle not in mydata2in:
                restful.abort(400)

            for clejson in valeur:
                if clejson not in mydata2in[cle]:
                    if (clejson == "client") or (clejson == "node"):
                        pass
                    else:
                        restful.abort(400)

                if clejson not in mydata2in[cle]:
                    pass
                else:
                    if mydata2in[cle][clejson] == "":
                        if (clejson == "client") or (clejson == "node"):
                            pass
                        else:
                            restful.abort(400)

        # --recuperation donnees
        for cle in JSON_IN:
            valeur_return = mydata2in[cle]
            break

    except Exception, myerror:
        APP.logger.error('def client_jsonrequest_verif  %s', myerror)
        restful.abort(500, message=myerror)

    return valeur_return
#______________________________________________________________________________
# CREATION invoice
#______________________________________________________________________________
def creation_invoice(data):
    """ def creation machine virtuel
    """
    APP.logger.info('def creation_vm')
    APP.logger.info(data)

    if 'action' not in data['invoice']:
        myaction = "adittion"
    else:
        myaction = data['invoice']['action']

    if 'client' not in data['invoice']:
        myclient = "client_test"
    else:
        myclient = data['invoice']['client']

    if 'node' not in data['invoice']:
        mynode = "node_test"
    else:
        mynode = data['invoice']['node']

    try:
        var1 = int(data['invoice']['var1'])
        var2 = int(data['invoice']['var2'])
    except:
        restful.abort(400)

    # ------verification------
    if var1 == "":
        restful.abort(400)

    if var2 == "":
        restful.abort(400)

    # ------action --------
    try:
        if (myaction.lower() == "adittion") or (myaction == "+"):
            resultat = int(var1) + int(var2)

        if (myaction.lower() == "multiplication") or (myaction == "*"):
            resultat = int(var1) * int(var2)

            # --------reponse serveur----------

        reponse_accords = [{"client": myclient,
                            "node": mynode,
                            "action": myaction,
                            "var1": var1,
                            "var2": var2,
                            "resultat": resultat,
                            "status": "200",
                            "message":"OK"
                            }
                           ]
    except:
        restful.abort(500)

    return data["invoice"], reponse_accords

#______________________________________________________________________________
# SERVER JSON RESPONSE OUTPUT
#______________________________________________________________________________

def server_json_response(mydata):
    """ server_json_response
    """
    APP.logger.info('def server_json_response')

    datatest = {'invoice': {'node': mydata}}

    return json.dumps(datatest, sort_keys=True)

#______________________________________________________________________________
# CLIENT REQUEST   <-->   SERVER RESPONSE
#______________________________________________________________________________

class ClientJsonRequest(restful.Resource):
    """ class ClientJsonRequest
        recuperation client request contenant JSON
    """
    def __init__(self):
        """init"""

    def post(self):
        """def post"""
        APP.logger.info('Debut POST : %s', datetime.now())
        APP.logger.info('---------------------------------------------')
        APP.logger.info('def ClientJsonRequest * POST ')

        # Recuperation JSON data envoye via curl / ...
        ### restful.Resource

        med_json_in = request.data

        # Conversion JSON data vers la structure Python
        data = json.loads(med_json_in)

        # Verification client request JSON
        client_jsonrequest_verif(data)

        # Authentification
        # auth_reponse = abort_si_user_non_existe(data)

        # Creation machine virtuelle -- Creation serveur RESPONSE
        datar, accords_data_json = creation_invoice(data)


        json_response = json.loads(server_json_response(accords_data_json))

        APP.logger.info('Fin POST : %s', datetime.now())
        return jsonify(json_response)


    def get(self):
        """def get"""
        APP.logger.info(datetime.now())
        APP.logger.error('def ClientJsonRequest * GET ')
        return restful.abort(400)

    def delete(self):
        """def delete"""
        APP.logger.info(datetime.now())
        APP.logger.error('def ClientJsonRequest * DELETE ')
        return restful.abort(400)

    def put(self):
        """def put"""
        APP.logger.info(datetime.now())
        APP.logger.error('def ClientJsonRequest * PUT ')
        return restful.abort(400)
#______________________________________________________________________________
# API REQUEST
#______________________________________________________________________________

API.add_resource(ClientJsonRequest, '/')

#______________________________________________________________________________
# MAIN
#______________________________________________________________________________

if __name__ == '__main__':
    APP.run(
        #host="0.0.0.0",
        port=int("5000")
    )
#______________________________________________________________________________


